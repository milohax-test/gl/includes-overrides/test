# test

Test include from templates with variable override

See [templates](https://gitlab.com/milohax-test/gl/includes-overrides/templates)

This is for [this Stack Overflow answer: "variables substitution (or overriding) when extends jobs from gitlab templates unsing include"](https://stackoverflow.com/questions/60153424/variables-substitution-or-overriding-when-extends-jobs-from-gitlab-templates-u/73004397#73004397)